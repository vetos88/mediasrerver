#!/usr/bin/env bash
# Менеджер управления сборкой. И запуском.
# ВАЖНО! Для обеспечения полной функциональности убедитесь что у вас есть доступ по ssh к серверам.
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'
# Список доступных команд. Команда передается первым аргументом.

commands=(
"build"
"run"
"stop"
)

remote_host="82.146.50.231"
IMAGE_NAME="mediaserver"

export IMAGE_NAME
function get_version {
    # Функция, которая спрашивает версию у пользователя.
    if [[  -n "$1" ]]; then
        USER_VERSION=$1
        echo -e "Version provided: ${YELLOW} ${USER_VERSION} ${NC}"
    else
        for (( ; ; ))
          do
            echo -e "Enter version:"
            read USER_VERSION
            if [[  -n "$USER_VERSION" ]]; then
                break
            else
                echo -e "${RED}Value not be empty${NC}"
            fi
          done
    fi
    export USER_VERSION
}

function push_image_to_host {
    # Отправление образов на сервер.
    tmp_dir=$(mktemp -d -t ci)
    echo -e "Backup image ${YELLOW}${IMAGE_NAME}:${USER_VERSION}${NC} to ${YELLOW}${tmp_dir}${NC} !"
    docker save -o ${tmp_dir}/${IMAGE_NAME}_${USER_VERSION} ${IMAGE_NAME}:${USER_VERSION}
    echo -e "Load to server ${YELLOW}${remote_host}${NC}!"
    scp ${tmp_dir}/${IMAGE_NAME}_${USER_VERSION}  vetos2@$remote_host:$config_path
    ssh vetos2@$remote_host "docker load -i ~/${IMAGE_NAME}_${USER_VERSION}"
    ssh vetos2@$remote_host "rm ~/${IMAGE_NAME}_${USER_VERSION}"
    echo -e "Image ${YELLOW}${IMAGE_NAME}:${USER_VERSION}${NC} on server!"
    #TODO поэксперимиентировать удалять или нет локальный образ в случаем помещения в репозиторий
    rm -rf $tmp_dir
    docker rmi ${IMAGE_NAME}:${USER_VERSION}
}

echo -e "${GREEN}$1 - command was provided.${NC}"
if [ "$1" == ${commands[0]} ]; then
  export $(cat .run-env)
  echo -e "${GREEN}New image will build.${NC}"
  get_version $2
  docker build . -t ${IMAGE_NAME}:${USER_VERSION} -f DockerfileMediaServer --no-cache
  for (( ; ; ))
  do
      echo -e "Do you want push image to host(image will remove locally)? host: ${YELLOW} ${remote_host} ${NC}(Y/n)(n)"
      read user_decision
      user_decision=${user_decision:-"n"}
      if [[ "$user_decision" == "Y" || "$user_decision" == "n" ]]; then
          break
      else
          echo -e "${RED}Y or n only!${NC}"
      fi
  done
  if [[ "$user_decision" == "Y" ]]; then
      push_image_to_host
  fi
  echo -e "${GREEN}Build complete! ${NC}"
elif [ "$1" == ${commands[1]} ]; then
  export $(cat .run-env)
  if [[  -z "$PERSISTANT_STATIC_FOLDER" ]]; then
    echo -e "${RED}Value PERSISTANT_STATIC_FOLDER not be empty. set it in .run-env file${NC}"
    exit 1
  else
    echo
    if [[ ! -d $PERSISTANT_STATIC_FOLDER ]]; then
      echo -e "${RED}Value $PERSISTANT_STATIC_FOLDER not exist.${NC}"
      exit 1
    fi
  fi
  get_version $2
  docker-compose run --rm $SERVICE_NAME collectstatic
  # Для того чтобы nginx видел статику
  chmod -R 744 ${PERSISTANT_STATIC_FOLDER}/**
  docker-compose run  --rm $SERVICE_NAME migrate
  docker-compose up -d
elif [ "$1" == ${commands[2]} ]; then
  export $(cat .run-env)
  docker-compose down
else
    echo -e "${RED}$0 $1 - Bad command provided!${NC} ${YELLOW}Provide one of: [${commands[*]}]. ${NC}"
fi
