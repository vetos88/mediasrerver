# MediaFIleServer

## Сборка базового образа python3.8
```bash
docker build . -t mediaserver:1 -f DockerfileMediaServer --no-cache
```

```bash
docker run --name mediaserver --rm --network="local-apps" -p 9080:8080 \
    -v /Users/u17783750/Documents/projects/mediaserver/mediaserver:/src \
    -d mediaserver:1
```

```bash
docker run --name mediaserver --rm --network="local-apps" \
    -v /home/gazeruser/gaserdata/media:/src/media \
    -v /home/gazeruser/gaserdata/jwt_keys:/jwtKeys \
    -d mediaserver:reports-6
```

```bash
docker run --name mediaserver --rm --network="local-apps" -p 9080:8080 \
    -v /Users/u17783750/Documents/projects/mediaserver/mediaserver:/src \
    -v /Users/u17783750/Documents/projects/study/viskill/emotion_keeper/jwtkeys:/jwtKeys \
    -d mediaserver:1
```




