"""
Менеджер работы с JWT токенами.
"""
import logging
import os

import jwt
from aiohttp import web
from aiohttp.web_middlewares import middleware
from jwt import InvalidTokenError

PYJWT_PUBLIC_KEY = os.path.join('/jwtKeys', os.environ.get('PYJWT_PUBLIC_KEY', 'jwtRS256.key.pub'))

logger = logging.getLogger('jst_producer')


class TokenProducer:
    def __init__(self):
        """
        Считывание ключей
        """
        with open(PYJWT_PUBLIC_KEY, 'rb') as public_key_file:
            self.public_key = public_key_file.read()

    def decode_key(self, jwt_token):
        """
        Расшифровка токена.
        :return:
        """
        decoded = jwt.decode(jwt_token, self.public_key, algorithms='RS256')
        return decoded


def jwt_middleware_factory(exception_and_point):
    jwt_token_producer = TokenProducer()

    def checkin_jwt(request):
        # TODO костыль чтобы можно было загружать видео
        if request.method == 'GET' and exception_and_point in str(request.rel_url):
            return False
        if request.method in ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']:
            return True

    @middleware
    async def jwt_middleware(request, handler):

        if checkin_jwt(request):
            try:
                jwt_token = request.headers['Authorization']
            except KeyError:
                raise web.HTTPForbidden(reason='jwt not provided')
            try:
                jwt_token = jwt_token.split(' ')[1]
            except IndexError:
                raise web.HTTPForbidden(reason='jwt bad format. provide Bearer token')
            try:
                user_payload = jwt_token_producer.decode_key(jwt_token)
            except InvalidTokenError as err:
                logging.warning('Provided bad token %s err %s', jwt_token, repr(err))
                raise web.HTTPUnauthorized(reason=repr(err))
        resp = await handler(request)
        return resp

    return jwt_middleware
