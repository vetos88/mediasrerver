"""
Главная точка входа в приложение.
"""
import logging
import urllib
import os
from datetime import datetime

from pathlib import Path
from uuid import uuid4

import aiohttp
import aiohttp_cors as aiohttp_cors
import aiohttp_jinja2
import jinja2
import uvloop
from aiofile import AIOFile, Writer

from aiohttp import web
from aiohttp_swagger import setup_swagger

from jwt_token_manager import jwt_middleware_factory

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger('recognitor')

WORK_DIR = os.getcwd()
TEMPLATE_PATH = Path(WORK_DIR, 'templates')
MEDIA_DIR = Path(WORK_DIR, 'media')
if not MEDIA_DIR.exists():
    raise ValueError('Please create MEDIA_DIR')
IMAGE_DIR = Path(MEDIA_DIR, 'image')
IMAGE_DIR.mkdir(exist_ok=True)
VIDEO_DIR = Path(MEDIA_DIR, 'video')
VIDEO_DIR.mkdir(exist_ok=True)
DOCUMENTS_DIR = Path(MEDIA_DIR, 'documents')
DOCUMENTS_DIR.mkdir(exist_ok=True)

RECEIVED_VIDEO = Path(VIDEO_DIR, 'received_video')
RECEIVED_VIDEO.mkdir(exist_ok=True)
CAMPAIGN_VIDEO = Path(VIDEO_DIR, 'campaign_video')
CAMPAIGN_VIDEO.mkdir(exist_ok=True)
PRODUCED_VIDEO = Path(VIDEO_DIR, 'produced_video')
PRODUCED_VIDEO.mkdir(exist_ok=True)
REPORTS_DIR = Path(DOCUMENTS_DIR, 'reports')
REPORTS_DIR.mkdir(exist_ok=True)
CAMPAIGN_IMAGES = Path(IMAGE_DIR, 'campaign_images')
CAMPAIGN_IMAGES.mkdir(exist_ok=True)
EMOTION_CHART = Path(IMAGE_DIR, 'emotion_chart')
EMOTION_CHART.mkdir(exist_ok=True)
PRODUCED_PICTURES = Path(IMAGE_DIR, 'produced_pictures')
PRODUCED_PICTURES.mkdir(exist_ok=True)


DOWNLOADING_URL_MEDIA_PATH = '/mediaserver/download/media'

app = web.Application(
    middlewares=[jwt_middleware_factory(DOWNLOADING_URL_MEDIA_PATH)]
)
aiohttp_jinja2.setup(
    app, loader=jinja2.FileSystemLoader(str(TEMPLATE_PATH))
)
cors = aiohttp_cors.setup(app)


@aiohttp_jinja2.template('index.html')
async def idexpage(request):
    """
    description: This end-point perform hard calculating actions.
    tags:
    - ControlPage
    produces:
    - text/plain
    responses:
        "200":
            description: html control page
    """
    return {}


def check_request_video_content(content_type):
    """
    Проверить тип загружaeмого файла.
    :return:
    """
    valid_content_types = {
        "video/mp4": "mp4",
        "video/webm": "webm",
        "video/mkv": "mkv",
        "video/quicktime": "mov"
    }
    try:
        video_extension = valid_content_types[content_type]
    except KeyError:
        raise aiohttp.web.HTTPBadRequest(
            reason='Provide valid content type: {}'.format(list(valid_content_types.keys()))
        )
    return video_extension


def check_request_picture_content(content_type):
    """
    Проверить тип загружaeмого файла.
    :return:
    """
    valid_content_types = {
        "image/jpeg": "jpeg",
        "image/jpg": "jpg",
        "image/bmp": "bmp",
        "image/png": "png"
    }
    try:
        img_extension = valid_content_types[content_type]
    except KeyError:
        raise aiohttp.web.HTTPBadRequest(
            reason='Provide valid content type: {}'.format(list(valid_content_types.keys()))
        )
    return img_extension


async def store_video_file(content, content_type, content_folder):
    """
    Загрузка и сохранение видеофайлов.
    :param content:
    :param content_type:
    :param content_folder:
    :return:
    """
    file_extension = check_request_video_content(content_type)
    file_url = await store_file(content, file_extension, content_folder)
    return file_url


async def store_picture_file(content, content_type, content_folder):
    """
    Загрузка и сохранение картинок.
    :param content:
    :param content_type:
    :param content_folder:
    :return:
    """
    file_extension = check_request_picture_content(content_type)
    file_url = await store_file(content, file_extension, content_folder)
    return file_url


async def store_file(content, file_extension, content_folder):
    """
    Загрузка и сохранение файла.
    :return:
    """
    size = 0
    date_folder = Path(content_folder, datetime.now().strftime("%Y%m"))
    date_folder.mkdir(exist_ok=True)
    received_file_name = f"{datetime.now().strftime('%d%H%M%S')}_{str(uuid4())}.{file_extension}"
    file_path = Path(date_folder, received_file_name)
    LOGGER.info("Start downloading file path [{}]".format(file_path))
    start_downloading_time = datetime.now()
    async with AIOFile(file_path, 'wb+') as afp:
        writer = Writer(afp)
        while True:
            chunk, _ = await content.readchunk()
            chunk_length = len(chunk)  # 8192 bytes by default.
            if not chunk:
                break
            size += chunk_length
            await writer(chunk)
        await afp.fsync()
    end_downloading_time = datetime.now()
    time_taken = end_downloading_time - start_downloading_time
    LOGGER.info(
        "Downloading file path: [{}] finished. time taken - {}. size {}".format(file_path, str(time_taken), size)
    )
    return urllib.parse.quote(
        str(file_path).replace(str(MEDIA_DIR), '', 1)
    )


async def upload_video(request):
    """
    ---
    description: Upload Video to Server.
    tags:
    - VideoUploading
    produces:
        - video
    requestBody:
      content:
        video/*:
          schema:
            type: string
            format: binary
    responses:
        "200":
            description: a successful video uploading. file path
        "400":
            bad content
    """
    file_url = await store_video_file(request.content, request.content_type, RECEIVED_VIDEO)

    return web.json_response({
        "result_video_url": f"{DOWNLOADING_URL_MEDIA_PATH}{file_url}"
    })


async def upload_campaign_video(request):
    """
    ---
    description: Upload Video to Server.
    tags:
    - VideoUploading
    produces:
        - video
    requestBody:
      content:
        video/*:
          schema:
            type: string
            format: binary
    responses:
        "200":
            description: a successful video uploading. file path
        "400":
            bad content
    """
    file_url = await store_video_file(request.content, request.content_type, CAMPAIGN_VIDEO)

    return web.json_response({
        "result_video_url": f"{DOWNLOADING_URL_MEDIA_PATH}{file_url}"
    })


async def upload_campaign_pictures_files(request):
    """
    ---
    description: Upload pictures to Server.
    tags:
    - PicuresUploading
    produces:
        - link
    requestBody:
      content:
        pic/*:
          schema:
            type: string
            format: binary
    responses:
        "200":
            description: a successful picture uploading. file path
        "400":
            bad content
    """
    file_url = await store_picture_file(request.content, request.content_type, CAMPAIGN_IMAGES)

    return web.json_response({
        "result_file_url": f"{DOWNLOADING_URL_MEDIA_PATH}{file_url}"
    })


upload_resource = cors.add(app.router.add_resource('/mediaserver/api/upload-video'))
cors.add(
    upload_resource.add_route("POST", upload_video),
    {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        ),
    }
)

upload_resource = cors.add(app.router.add_resource('/mediaserver/api/upload-campaign-video'))
cors.add(
    upload_resource.add_route("POST", upload_campaign_video),
    {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        ),
    }
)

upload_resource = cors.add(app.router.add_resource('/mediaserver/api/upload-campaign-pictures-files'))
cors.add(
    upload_resource.add_route("POST", upload_campaign_pictures_files),
    {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        ),
    }
)

app.add_routes([web.static(f'{DOWNLOADING_URL_MEDIA_PATH}', str(MEDIA_DIR), show_index=False)])
app.add_routes([web.static('/mediaserver/file-browser', str(MEDIA_DIR), show_index=True)])

setup_swagger(app, swagger_url='swagger')
app.add_routes([web.get('/mediaserver', idexpage)])

if __name__ == "__main__":
    uvloop.install()
    web.run_app(app)
